<?php

/**
 * The plugin bootstrap file
 *
 *
 * @link              https://www.intermedia.com.au/
 * @since             2.0.0
 * @package           Intermedia_Polar_Integration
 *
 * @wordpress-plugin
 * Plugin Name:       Intermedia Polar Integration 
 * Plugin URI:        https://www.intermedia.com.au/
 * Description:       The Intermedia Polar Integration WordPress Plugin lets you deploy the MediaVoice Scripts on the website.
 * Version:           2.0.0
 * Author:            Lu Chen / Jose Anton
 * Author URI:        https://www.intermedia.com.au/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       intermedia-polar-integration
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'INTERMEDIA_POLAR_INTEGRATION_VERSION', '2.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-intermedia-polar-integration-activator.php
 */
function activate_intermedia_polar_integration() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-intermedia-polar-integration-activator.php';
	Intermedia_Polar_Integration_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-intermedia-polar-integration-deactivator.php
 */
function deactivate_intermedia_polar_integration() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-intermedia-polar-integration-deactivator.php';
	Intermedia_Polar_Integration_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_intermedia_polar_integration' );
register_deactivation_hook( __FILE__, 'deactivate_intermedia_polar_integration' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-intermedia-polar-integration.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_intermedia_polar_integration() {

	$plugin = new Intermedia_Polar_Integration();
	$plugin->run();

}
run_intermedia_polar_integration();
