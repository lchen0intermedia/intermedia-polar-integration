<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.intermedia.com.au/
 * @since      1.0.0
 *
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/public
 * @author     Intermedia <Intermedia@intermedia.com.au>
 */
class Intermedia_Polar_Integration_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Intermedia_Polar_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Intermedia_Polar_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$cssFilePath = glob(plugin_dir_path( __DIR__ ) . 'build/public/css/build.min.*');
		$cssFileURI = plugin_dir_url( __DIR__ ) . 'build/public/css/' . basename($cssFilePath[0]);
		wp_enqueue_style( $this->plugin_name, $cssFileURI, array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Intermedia_Polar_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Intermedia_Polar_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        $jsFilePath = glob(plugin_dir_path( __DIR__ ) . 'build/public/js/build.min.*');
		$jsFileURI = plugin_dir_url( __DIR__ ) . 'build/public/js/' . basename($jsFilePath[0]);
		wp_enqueue_script( $this->plugin_name, $jsFileURI, $this->version, false );

	}
        
        private static function compiled_template_dfp () {
            
            $compiled_templates='';
            
            for($i = 1; $i <= 4; $i++) {
                
                $compiled_templates .= 'var compiledTemplate'.$i.' = "";';
                
            }
            
            return $compiled_templates;
            
        }
        
        private static function get_option_polar_dfp () {
            
            for($i = 1; $i <= 4; $i++) { 
            
                if(!empty(get_option('polar_'.$i.'_dfp_id')) && !empty(get_option('polar_'.$i.'_location'))) {
                    
                    echo '	
                    q.push(["insertPreview", {
                        label: "Home",
                        unit: {"server":"dfp","id":"'.get_option('polar_'.$i.'_dfp_id').'","size":"2x2","targets":{}},
                        location: "'.get_option('polar_'.$i.'_location').'",
                        infoText: "",
                        infoButtonText: "",
                    ';
                    
                    if ( !empty( get_option('polar_'.$i.'_template') ) ) {
                        
                        echo 'template: compiledTemplate'.$i.',';
                        
                    } else {
                        
                        echo 'template: compiledTemplate,';
                        
                    }
                    
                    echo 'onRender: function(element, data){ ';
                    
                    if ( !empty(get_option('polar_'.$i.'_video') ) ) {
                        
                        echo 'jQuery("#polar_video").html(\''.addslashes(str_replace('</script>', '<\/script>', str_replace("\n", "", get_option('polar_'.$i.'_video')))).'\');';
                     
                    }
                    echo'
                        if(typeof gtag == "function") {  
                            gtag("event", "Impression", { 
                                "event_category": "Sponsor Content Block Impression",
                                "event_label": data.title + ":" + data.sponsor.name, 
                                "value": 1
                            });                                                        

                        } else if (typeof ga == "function") {
                        
                            ga("send", "event", "Sponsor Content Block Impression","Impression",data.title + ":" + data.sponsor.name,1);                                                       
                                                            }
                        },
                        onFill: function(data) { 

                        },
                        onError: function(error) {

                        jQuery("'.get_option('polar_'.$i.'_location').'").remove();
                            console.log(error);}
                    }]);';
                }
            }
        }
        
        private static function get_option_polar_template () {
            
            for($i = 1; $i <= 4; $i++) {
                
                if( !empty( get_option('polar_'.$i.'_template') ) ) {
                    
                    echo 'compiledTemplate'.$i.' = '.str_replace("'", '"', get_option('polar_'.$i.'_template')).';';
                    
                }
                
            }
            
        }
        
        public static function render_intermedia_polar_content () {
            
            if ( is_home() || is_front_page() ) : ?>
                
                    <!-- This is the render function for Polar -->
                    <script type="text/javascript">
                        
                        (function(){

                            var compiledTemplate = "";
                            
                            /* Starts Function compiled_template_dfp */
                            <?php echo self::compiled_template_dfp (); ?>
                            /* Ends Function compiled_template_dfp */

                            templates();
                            
                            window.NATIVEADS = window.NATIVEADS || {};
                            window.NATIVEADS_QUEUE = window.NATIVEADS_QUEUE || [];
                            var q = window.NATIVEADS_QUEUE;
                            q.push(["setPropertyID", "<?php echo get_option('polar_property_id'); ?>"]);
                            q.push(["injectCSS", " ", "head"]);
                            
                            /* Starts Function get_option_polar_dfp */
                            <?php echo self::get_option_polar_dfp(); ?>
                            /* Ends Function get_option_polar_dfp */

                            function templates () {
                                
                                /* Starts Function get_option_polar_template */
                                <?php echo self::get_option_polar_template(); ?>
                                /* Ends Function get_option_polar_template */

                                compiledTemplate = <?php echo str_replace("'", '"', get_option('polar_compiled_template')) ?>;

                            }

                        })();

                        (function(){

                            var parallaxCompiledTemplate = "";
                            templates();
                            window.NATIVEADS = window.NATIVEADS || {};
                            window.NATIVEADS_QUEUE = window.NATIVEADS_QUEUE || [];
                            var q = window.NATIVEADS_QUEUE;
                            q.push(["setPropertyID", "<?php echo get_option('polar_property_id'); ?>"]);
                            q.push(["injectCSS", " ", "head"]);
                            
                            <?php if( !empty( get_option('parallax_compiled_template') ) ) : ?>
                                /* Starts Function compiled_template_parallax */
                                q.push(["insertPreview", {
                                    label: "Home",
                                    unit: {"server":"dfp","id":"<?php echo get_option('parallax_dfp_id')?>","size":"2x2","targets":{}},
                                    location: "#parallaxContent",
                                    infoText: "",
                                    infoButtonText: "",
                                    template: parallaxCompiledTemplate,
                                    onRender: function(element, data) {
                                        if( typeof gtag == "function" ) {  
                                            gtag(
                                                "event", 
                                                "Impression", 
                                                { 
                                                    "event_category": "Sponsor Content Block Impression",
                                                    "event_label": "parallax_sponsored_spot:" + data.title + ":" + data.sponsor.name, 
                                                    "value": 1
                                                }
                                            );
                                        } else if ( typeof ga == "function" ) {
                                            ga(
                                                "send", 
                                                "event", 
                                                "Sponsor Content Block Impression",
                                                "Impression",
                                                "parallax_sponsored_spot:" + data.title + ":" + data.sponsor.name,1
                                            );                                                       
                                        }
                                    },
                                    onFill: function(data) {},
                                    onError: function(error) {
                                        jQuery("#parallaxContent").remove();
                                        console.log(error);
                                    }
                                }]);
                                /* Ends Function compiled_template_parallax */
                            <?php endif; ?>
                        
                            function templates () {
                                
                            <?php if ( !empty( get_option('parallax_compiled_template') ) ) : ?>

                                parallaxCompiledTemplate = <?php echo str_replace("'", '"', get_option('parallax_compiled_template')) ?>;

                            <?php endif; ?>

                            }
                            
                        })();

                        (function(d, s, id) {

                            var js, fjs = d.getElementsByTagName(s)[0], p = d.location.protocol;
                            if (d.getElementById(id)) {return;}
                            js = d.createElement(s);
                            js.id = id; js.type = "text/javascript"; js.async = true;
                            js.src = ((p == "https:") ? p : "http:") + "//plugin.mediavoice.com/plugin.js";
                            fjs.parentNode.insertBefore(js, fjs);

                        })(document, "script", "nativeads-plugin");
                    
                    </script>
                    <!-- This is the end of the render function for Polar -->
                    
                    <!-- Dependencies for Parallax -->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.4/TweenMax.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/animation.gsap.js"></script>
                    <!-- Starts Instance for the Parallax section -->
                    <script>
                        document.onreadystatechange = function(){
                            
                            if (document.readyState === "complete") {

                                (function($){
                                    //init scrollmagic
                                    var controller = new ScrollMagic.Controller();
                                    var checkExist = setInterval(function() {
                                    if ($('.slide-parallax.mediavoice-native-ad').length) {
                                        console.log("Parallax activated");
                                        clearInterval(checkExist);
                                        $(".slide-parallax.mediavoice-native-ad").each( function () {
                                        var bg = $(this).find(".bg");
                                        // Add tweenmax for backgroundParallax
                                        var parallax = TweenMax.from( bg, 1, {
                                                y: '-50%',
                                                ease: Power0.easeNone
                                        } );
                                        // Create scrollmagic scene
                                        var parallaxScene = new ScrollMagic.Scene({
                                                triggerElement: this, // <-- Use this to select current element
                                                triggerHook: 1,
                                                duration: '200%'
                                        })
                                        .setTween( parallax )
                                        .addTo(controller);
                                    });
                                    }
                                    }, 100); // check every 100ms
                                })(jQuery);
                            }
                        };
                    </script>
                    <!-- Ends Instance for the Parallax section -->
                    
            <?php   endif;  
              
	}
        
        public static function intermedia_polar_parallax_shortcode () {
            
            return '<div id="parallaxContent"></div>';
            
        }
        
        public function register_shortcodes() {
            
            add_shortcode( 'polar_parallax', array( $this, 'intermedia_polar_parallax_shortcode') );
            
        }
        public static function tracking_sponsored_content()
        {
              //sponsoredcontent
              if ( is_singular( array( 'sponsoredcontent') ) || is_page_template('sponsored-content-template.php')) {
                echo '<script async src="//cdn.mediavoice.com/nativeads/script/intermedia/analytics_tag.js"></script>';
            }
        }

        public function add_sponsor_content_into_search( $query ) 
        {
                    if ( $query->is_search && !is_admin()) {
                        $query->set( 'post_type', array('ai1ec_event', 'post', 'roundtable', 'sponsoredcontent', 'job_listing') );
                    }
                
                    return $query;
        }
        	
        public function add_sponsor_content_into_category($query) 
        {
                    if( is_category() ) {
                        $post_type = get_query_var('post_type');
                        if($post_type) {
                            $post_type = $post_type;
                        } else {
                            $post_type = get_post_types(); 
                        }
                        $query->set('post_type',$post_type);
                        return $query;
                    }
        }
        	
        public function add_sponsor_content_into_tag($query) 
        {
                    if( is_tag() ) {
                        $post_type = get_query_var('post_type');
                        if($post_type) {
                            $post_type = $post_type;
                        }else {
                            $post_type = get_post_types(); 
                        }
                        $query->set('post_type',$post_type);
                        return $query;
                    }
        }
}
