# Welcome to Intermedia Polar Integration Plugin

  

The Intermedia Polar Integration WordPress Plugin lets you deploy the MediaVoice Scripts on the website.

  

# How it works

The Intermedia Polar Integration WordPress Plugin lets you deploy the MediaVoice Scripts on the website.

  

>  **Tip:**  **Intermedia Polar Integration WordPress Plugin** uses the **Enqueue** method to load and sort the CSS file the right way instead of the old **@import** method...

>

## How to install Intermedia Polar Integration WordPress Plugin on your site

  

The easiest way to do so is to [download the latest plugin release]([https://bitbucket.org/jantonca/intermedia-polar-integration/downloads/](https://bitbucket.org/jantonca/intermedia-polar-integration/downloads/)). Upload them using the plugin installer in your WordPress admin interface.

  

## Editing

### Admin Interface
Add your own CSS styles to `admin/css/src/app.scss` or import you own files into `/css/src/main.scss`

To write scss variables just add your own value to: `admin/css/variables/_variables.scss`. 

Add your own color like: `$color__primary: #dddddd;` in `admin/css/variables/_variables.scss` . This change will automatically apply to all elements that use the $color__primary variable.

It will be outputted into: `build/admin/css/build.min.[hash].css`

So you have one clean CSS file at the end and just one request.

  

Add your own JS scripts to `admin/js/src/app.js`

  

It will be outputted into: `build/admin/js/build.min.[hash].js`

### Public Interface
Add your own CSS styles to `public/css/src/app.scss` or import you own files into `/css/src/main.scss`

To write scss variables just add your own value to: `public/css/variables/_variables.scss`. 

Add your own color like: `$color__primary: #dddddd;` in `public/css/variables/_variables.scss` . This change will automatically apply to all elements that use the $color__primary variable.

It will be outputted into: `build/public/css/build.min.[hash].css`

So you have one clean CSS file at the end and just one request.

  

Add your own JS scripts to `public/js/src/app.js`

  

It will be outputted into: `build/public/js/build.min.[hash].js`
  

## Developing With NPM, Webpack and SASS

  

### Installing Dependencies

  

- Make sure you have installed Node.js on your computer globally

- Open your terminal and browse to the location of your Child theme copy

- Run: `$ npm install`

  

### Running

  

To work and compile your Sass and JS files on the fly start:

  

-  `$ npx webpack --watch`

  
  

## Organizing the Source Code

  

I created a **JS** directory and a **CSS** directory inside of the `public` and `admin` folder. Each of these directories had a `vendor` folder for adding third party libraries. All of them contain the source code. The `build` folder, with `public` and `admin` sub folders, acts as the build target for the **Webpack** compiled assets.