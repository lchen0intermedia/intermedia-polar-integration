<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.intermedia.com.au/
 * @since      1.0.0
 *
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/includes
 * @author     Intermedia <Intermedia@intermedia.com.au>
 */
class Intermedia_Polar_Integration_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
