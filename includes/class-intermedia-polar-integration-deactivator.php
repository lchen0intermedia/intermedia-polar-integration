<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.intermedia.com.au/
 * @since      1.0.0
 *
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/includes
 * @author     Intermedia <Intermedia@intermedia.com.au>
 */
class Intermedia_Polar_Integration_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
