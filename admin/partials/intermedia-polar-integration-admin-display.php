<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.intermedia.com.au/
 * @since      1.0.0
 *
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">

    <h1><?php _e( 'Intermedia Polar Integration', 'intermedia-polar-integration' ); ?></h1>
    
    <hr>
    
    <form method="post" action="options.php">

        <?php settings_fields( 'intermedia-polar-settings-group' ); ?>
        
        <?php do_settings_sections( 'intermedia-polar-settings-group' ); ?>

        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">Polar Property ID</th>
                    <td>
                        <input class="input-custom" type="text" name="polar_property_id" value="<?php echo esc_attr( get_option('polar_property_id') ); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Parallax DFP ID</th>
                    <td>
                        <input class="input-custom" type="text" name="parallax_dfp_id" value="<?php echo esc_attr( get_option('parallax_dfp_id') ); ?>" />
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <?php for($i = 1; $i <= 4; $i++): ?>
        <h2>Polar DFP <?php echo $i; ?> Settings</h2>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">Polar <?php echo $i; ?> DFP ID</th>
                    <td>
                        <input class="input-custom" type="text" name="polar_<?php echo $i; ?>_dfp_id" value="<?php echo esc_attr( get_option('polar_'.$i.'_dfp_id') ); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Polar <?php echo $i; ?> Location</th>
                    <td>
                        <input class="input-custom" type="text" name="polar_<?php echo $i; ?>_location" value="<?php echo esc_attr( get_option('polar_'.$i.'_location') ); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Polar <?php echo $i; ?> Template</th>
                    <td>
                        <textarea class="text-area-custom" name="polar_<?php echo $i; ?>_template" />
                            <?php echo esc_attr( get_option('polar_'.$i.'_template') ); ?>
                        </textarea>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Polar <?php echo $i; ?> Video</th>
                    <td>
                        <textarea class="text-area-custom" name="polar_<?php echo $i; ?>_video" />
                            <?php echo esc_attr( get_option('polar_'.$i.'_video') ); ?>
                        </textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <?php endfor; ?>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">Compiled Template</th>
                    <td>
                        <textarea class="text-area-custom" name="polar_compiled_template" />
                            <?php echo esc_attr( get_option('polar_compiled_template') ); ?>
                        </textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">Compiled Parallax Template</th>
                    <td>
                        <textarea class="text-area-custom" name="parallax_compiled_template" />
                            <?php echo esc_attr( get_option('parallax_compiled_template') ); ?>
                        </textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>In order to make it work in a template the Shortcode <code>[polar_parallax]</code> should be added to the design.</p>
        <hr>
        
        <?php submit_button(); ?>

    </form>
    
</div>
