<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.intermedia.com.au/
 * @since      1.0.0
 *
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Intermedia_Polar_Integration
 * @subpackage Intermedia_Polar_Integration/admin
 * @author     Intermedia <Intermedia@intermedia.com.au>
 */
class Intermedia_Polar_Integration_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Intermedia_Polar_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Intermedia_Polar_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		$cssFilePath = glob(plugin_dir_path( __DIR__ ) . 'build/admin/css/build.min.*');
		$cssFileURI = plugin_dir_url( __DIR__ ) . 'build/admin/css/' . basename($cssFilePath[0]);
		wp_enqueue_style( $this->plugin_name, $cssFileURI, array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Intermedia_Polar_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Intermedia_Polar_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( 'autosize', plugin_dir_url( __FILE__ ) . 'js/vendor/autosize.min.js', array(), $this->version, false );
		$jsFilePath = glob(plugin_dir_path( __DIR__ ) . 'build/admin/js/build.min.*');
		$jsFileURI = plugin_dir_url( __DIR__ ) . 'build/admin/js/' . basename($jsFilePath[0]);
		wp_enqueue_script( $this->plugin_name, $jsFileURI, array( 'jquery' ), $this->version, true );

	}
        // create custom plugin settings menu


        public function intermedia_polar_create_menu() {

            //create new top-level menu
            add_menu_page (
                'Intermedia Polar Integration', 
                'Intermedia Polar Integration', 
                'administrator', __FILE__, 
                array( $this, 'load_admin_page_content' ),
                plugins_url( 'intermedia-polar-integration/images/Polar_Logo_White_15x25.png' )
            );
            
        }
        
        public function register_intermedia_polar_settings() {
            
            //register our settings
            register_setting( 'intermedia-polar-settings-group', 'polar_property_id' );
			register_setting( 'intermedia-polar-settings-group', 'parallax_dfp_id' );
			register_setting( 'intermedia-polar-settings-group', 'polar_compiled_template' );
            register_setting( 'intermedia-polar-settings-group', 'parallax_compiled_template' );
            
            for($i = 1; $i <= 4; $i++):
                
                register_setting( 'intermedia-polar-settings-group', 'polar_'.$i.'_dfp_id' );
                register_setting( 'intermedia-polar-settings-group', 'polar_'.$i.'_location' );
                register_setting( 'intermedia-polar-settings-group', 'polar_'.$i.'_template' );
                
            endfor;
            
        }
        
        // Load the plugin admin page partial.
        public function load_admin_page_content() {
            
            require_once plugin_dir_path( __FILE__ ). 'partials/intermedia-polar-integration-admin-display.php';
            
		}
		public static function render_dfp_meta_box_for_sponsor_content()
		{
			global $post; 
			//get current post dfp tag 
			$post_dfp_tag = get_post_meta($post->ID, 'post_dfp_tag', true);
			
			//get the dfp tag list 
			$dfp_tags = get_option('dfp_tags');
			if(!empty($dfp_tags)) {	
				if(!empty($post_dfp_tag)) {
					echo 'DFP Tag: ' . $post_dfp_tag . '<br/>';
				}else {
					echo 'Not set DFP Tag yet.';
				}
				echo '<input type="hidden" value="'.wp_create_nonce('dfp_post_tag'.$post->ID).'" name="dfp_post_nonce">';
				echo '<select name="post_dfp_select" style="width: 260px;"><option value=""></option>';
				foreach($dfp_tags as $dfp_tag) {
					if(!empty($post_dfp_tag) && $post_dfp_tag == $dfp_tag) {
						echo '<option value="'.$dfp_tag.'" selected>'.$dfp_tag.'</option>';
					}else {
						echo '<option value="'.$dfp_tag.'">'.$dfp_tag.'</option>';
					}
				} 
				echo '</select>';
			}else {
				echo '<h4>There is no DFP Tags defined.</h4>';
			}
			if(empty($dfp_tags) && !empty($post_dfp_tag)) {
				echo 'post dfp: ' . $post_dfp_tag . '<br/>';
			}
		}
	
		public static function init() 
		{
			add_action('admin_menu', array(get_called_class(), 'sponsor_content_menu'));
		}
	
		public static function sponsor_content_menu() 
		{
			add_menu_page('Sponsor Content Setting', 'Sponsor Content Setting', 'edit_others_posts', 'sponsor_content_setting', array('IntermediaGroup_SponsorContent', 'render_option_page'), '', 150);
		}
	
		public static function create_sponsoredcontent_cpt() 
		{
			$labels = array(
				'name' => __( 'Sponsored Contents', 'Post Type General Name', 'intermedia' ),
				'singular_name' => __( 'Sponsored Content', 'Post Type Singular Name', 'intermedia' ),
				'menu_name' => __( 'Sponsored Contents', 'intermedia' ),
				'name_admin_bar' => __( 'Sponsored Content', 'intermedia' ),
				'archives' => __( 'Sponsored Content Archives', 'intermedia' ),
				'attributes' => __( 'Sponsored Content Attributes', 'intermedia' ),
				'parent_item_colon' => __( 'Parent Sponsored Content:', 'intermedia' ),
				'all_items' => __( 'All Sponsored Contents', 'intermedia' ),
				'add_new_item' => __( 'Add New Sponsored Content', 'intermedia' ),
				'add_new' => __( 'Add New', 'intermedia' ),
				'new_item' => __( 'New Sponsored Content', 'intermedia' ),
				'edit_item' => __( 'Edit Sponsored Content', 'intermedia' ),
				'update_item' => __( 'Update Sponsored Content', 'intermedia' ),
				'view_item' => __( 'View Sponsored Content', 'intermedia' ),
				'view_items' => __( 'View Sponsored Contents', 'intermedia' ),
				'search_items' => __( 'Search Sponsored Content', 'intermedia' ),
				'not_found' => __( 'Not found', 'intermedia' ),
				'not_found_in_trash' => __( 'Not found in Trash', 'intermedia' ),
				'featured_image' => __( 'Featured Image', 'intermedia' ),
				'set_featured_image' => __( 'Set featured image', 'intermedia' ),
				'remove_featured_image' => __( 'Remove featured image', 'intermedia' ),
				'use_featured_image' => __( 'Use as featured image', 'intermedia' ),
				'insert_into_item' => __( 'Insert into Sponsored Content', 'intermedia' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Sponsored Content', 'intermedia' ),
				'items_list' => __( 'Sponsored Contents list', 'intermedia' ),
				'items_list_navigation' => __( 'Sponsored Contents list navigation', 'intermedia' ),
				'filter_items_list' => __( 'Filter Sponsored Contents list', 'intermedia' ),
			);
			$args = array(
				'label' => __( 'Sponsored Content', 'intermedia' ),
				'description' => __( 'Third party Content', 'intermedia' ),
				'labels' => $labels,
				'menu_icon' => 'dashicons-feedback',
				'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'page-attributes', 'post-formats', 'custom-fields', ),
				'taxonomies' => array('category', 'post_tag'),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'menu_position' => 5,
				'show_in_admin_bar' => true,
				'show_in_nav_menus' => true,
				'can_export' => true,
				'has_archive' => true,
				'hierarchical' => false,
				'exclude_from_search' => false,
				'show_in_rest' => true,
				'publicly_queryable' => true,
				'capability_type' => 'post',
			);
			register_post_type( 'sponsoredcontent', $args );
		}

		public function add_dfp_meta_box_into_post() 
		{
			if(current_user_can('edit_others_posts')) {
				  add_meta_box('dfp-tag-select', __('DFP Tag'), array('Intermedia_Polar_Integration_Admin', 'render_dfp_meta_box_for_sponsor_content'), 'sponsoredcontent', 'side');
			}
		}
		

	/*******************************METABOXES SECTION***********************************************/

	public static function display_sponsored_content_meta_box() {

		add_meta_box(
			'display_sponsored_content_data',
			__( 'Sponsored Content Data', 'intermedia' ),
			array( 'Intermedia_Polar_Integration_Admin', 'display_sponsored_content_meta_box_callback' ),
			'sponsoredcontent',
			'side',
			'low'
				
		);

	}
	/****** META BOXES DISPLAY ***********/
	public static function display_sponsored_content_meta_box_callback ( $post ) {

		// Add a nonce field so we can check for it later.
		// wp_nonce_field( 'display_sponsored_content_nonce', 'display_sponsored_content_nonce' );

		$value_sponsored_content_name = get_post_meta( $post->ID, 'intermedia_sponsored_content_name', true );
		$value_sponsored_content_url = get_post_meta( $post->ID, 'intermedia_sponsored_content_url', true );
		
		?>

		<label for="intermedia_sponsored_content_name">Add the name of the sponsor.</label>
			
		<input 
			style="width:100%;margin-top: 10px;" 
			id="intermedia_sponsored_content_name" 
			class="input-custom" 
			name="intermedia_sponsored_content_name" 
			value="<?php echo esc_attr( $value_sponsored_content_name ); ?>" 
		/>

		<label style="width:100%;margin-top: 20px;"  for="intermedia_sponsored_content_url">Add the link of the sponsored content</label>

		<input 
			style="width:100%;margin-top: 10px;" 
			id="intermedia_sponsored_content_url" 
			name="intermedia_sponsored_content_url" 
			class="input-custom" 
			value="<?php echo esc_attr( $value_sponsored_content_url ); ?>" 
		/>
		
		<?php
		
	}
	/****** META BOXES SAVE DATA ***********/
	public static function display_sponsored_content_meta_box_save_postdata($post_id) {
		if ( filter_input_array( INPUT_POST )) {

			if (array_key_exists( 'intermedia_sponsored_content_name', filter_input_array( INPUT_POST ) ) ) {
				$sponsored_content_name = filter_input( INPUT_POST, 'intermedia_sponsored_content_name' );
				update_post_meta(
					$post_id,
					'intermedia_sponsored_content_name',
					$sponsored_content_name
				);
				
			}

			if (array_key_exists('intermedia_sponsored_content_url', filter_input_array( INPUT_POST ) ) ) {
				$sponsored_content_url = ltrim( filter_input( INPUT_POST, 'intermedia_sponsored_content_url' ) );
				update_post_meta(
					$post_id,
					'intermedia_sponsored_content_url',
					$sponsored_content_url
				);
				
			}
		}
	}
}
