const path = require('path');

// include the js minification plugin
//const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");

// include the css extraction and minification plugins
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

//const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: {
    admin: ['./admin/js/app.js', './admin/css/main.scss'],
    public: ['./public/js/app.js', './public/css/main.scss']
  },
  output: {
    filename: './build/[name]/js/build.min.[fullhash].js',
    path: path.resolve(__dirname)
  },
  module: {
    rules: [
      // perform js babelization on all .js files
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ['babel-preset-env']
         }
        }
      },
      // compile all .scss files to plain old css
      {
        test: /\.(sass|scss)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    // extract css into dedicated file
    new MiniCssExtractPlugin({
      filename: './build/[name]/css/build.min.[fullhash].css'
    }),
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ['./build/[name]/js/*','./build/[name]/css/*']
    })

  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
      }),
      // enable the css minification plugin
      //new OptimizeCSSAssetsPlugin({})
      new CssMinimizerPlugin(),
    ],
  }
};